- Ключевое слово `ORDER BY`
  Сортировка данных в `SELECT`
  Возможна сортировка по одному или нескольким столбцам

- Порядок сортировки
  `ASC` (ascending) – сортировка по возрастанию (используется по умолчанию)
  `DESC` (descending) – сортировка по убыванию

- Совместное использование ключевых слов в `SELECT`
  `WHERE`, `ORDER BY`, `LIMIT`

- Синтаксис `SQL` похож на английский язык
  `SEQUEL` (Structured English Query Language)


## Сортировка в SQL: ORDER BY

в SQL выборка данных из таблиц делается через оператор SELECT. при этом
порядок перечисления произвольный, никакой гарантии порядка извлечения строк
из таблицы нет. При нескольких запусках одного и того же оператора SELECT есть
вероятность что извлечённые данные могут быть в разном порядке( такое бывает
редко, но теоретически возможно). Для гарантирования порядка нужно использовать
сортировку данных.
`ORDER BY` - ключевое слово позволяющее сортировать извлекаемые данные.
`ORDER BY` обычно записывается после ключевого слова `FROM`

сортировать данные можно по любому столбцу таблицы.

Выборка всех персонажей с сортировкой по столбцу year(год первого появления)
```sql
SELECT *
FROM superheroes
ORDER BY year;
```

```
  id  |             name             |        align       |     eye    |    hair    |       gender      | appearances | year | universe
------+------------------------------+--------------------+------------+------------+-------------------+-------------+------+----------
 5352 | Arthur Pendragon (New Earth) | Good Characters    | Brown Eyes | Brown Hair | Male Characters   | 41          | 1936 | dc
 5959 | Lady of the Lake (New Earth) | Good Characters    | Blue Eyes  | Blue Hair  | Female Characters | 13          | 1936 | dc
 5094 | Merlin (New Earth)           | Neutral Characters | Black Eyes | White Hair | Male Characters   | 92          | 1936 | dc
 5143 | Cyril Saunders (New Earth)   | Good Characters    | Hazel Eyes | White Hair | Male Characters   | 79          | 1937 | dc
 4941 | Samuel Bradley (New Earth)   | Good Characters    | Grey Eyes  | Grey Hair  | Male Characters   | 213         | 1937 | dc
 6087 | Rose Psychic (New Earth)     | Good Characters    | Black Eyes | Black Hair | Female Characters | 11          | 1937 | dc
...
 1858 | Daman Veteri (Earth-616)     | Neutral Characters | Black Eyes | Black Hair | Male Characters   | 13          | 2013 | marvel
(7281 rows)
```


### Порядки сортировки
- Порядки сортировки в `ORDER BY`
  - `ASC`  (ascending) - сортировка по возрастанию (используется по умолчанию
  - `DESC` (descending) - сортировка по убыванию

сортируем по популярности
```sql
SELECT *
FROM superheroes
ORDER BY appearances DESC;
```

appearances - столбец хранящий количество появления персонажа в комиксах
DESC - сортировка по убыванию

```
  id  |                 name                |        align       |     eye    |    hair    |      gender     | appearances | year | universe
------+-------------------------------------+--------------------+------------+------------+-----------------+-------------+------+----------
 1    | Spider-Man (Peter Parker)           | Good Characters    | Hazel Eyes | Brown Hair | Male Characters | 4043        | 1962 | marvel
 2    | Captain America (Steven Rogers)     | Good Characters    | Blue Eyes  | White Hair | Male Characters | 3360        | 1941 | marvel
 4854 | Batman (Bruce Wayne)                | Good Characters    | Blue Eyes  | Black Hair | Male Characters | 3093        | 1939 | dc
 3    | Wolverine (James \"Logan\" Howlett) | Neutral Characters | Blue Eyes  | Black Hair | Male Characters | 3061        | 1974 | marvel
 4    | Iron Man (Anthony \"Tony\" Stark)   | Good Characters    | Blue Eyes  | Black Hair | Male Characters | 2961        | 1963 | marvel
 4855 | Superman (Clark Kent)               | Good Characters    | Blue Eyes  | Black Hair | Male Characters | 2496        | 1986 | dc
 5    | Thor (Thor Odinson)                 | Good Characters    | Blue Eyes  | Blond Hair | Male Characters | 2258        | 1950 | marvel
 6    | Benjamin Grimm (Earth-616)          | Good Characters    | Blue Eyes  | No Hair    | Male Characters | 2255        | 1961 | marvel
 7    | Reed Richards (Earth-616)           | Good Characters    | Brown Eyes | Brown Hair | Male Characters | 2072        | 1961 | marvel
 8    | Hulk (Robert Bruce Banner)          | Good Characters    | Brown Eyes | Brown Hair | Male Characters | 2017        | 1962 | marvel
 9    | Scott Summers (Earth-616)           | Neutral Characters | Brown Eyes | Brown Hair | Male Characters | 1955        | 1963 | marvel
 10   | Jonathan Storm (Earth-616)          | Good Characters    | Blue Eyes  | Blond Hair | Male Characters | 1934        | 1961 | marvel
```



### Одновременная Сортировка и Фильтры в SELECT

выборка самых популярных отрицательных персонажей
```sql
SELECT *
FROM superheroes
WHERE align = 'Bad Characters'
ORDER BY appearances DESC;
```

```
  id  |             name             |      align     |     eye     |     hair    |       gender      | appearances | year | universe
------+------------------------------+----------------+-------------+-------------+-------------------+-------------+------+----------
 42   | Victor von Doom (Earth-616)  | Bad Characters | Brown Eyes  | Brown Hair  | Male Characters   | 721         | 1962 | marvel
 46   | Norman Osborn (Earth-616)    | Bad Characters | Blue Eyes   | Auburn Hair | Male Characters   | 692         | 1964 | marvel
 4880 | Joker (New Earth)            | Bad Characters | Green Eyes  | Green Hair  | Male Characters   | 517         | 1940 | dc
 68   | Wilson Fisk (Earth-616)      | Bad Characters | Blue Eyes   | Bald        | Male Characters   | 503         | 1967 | marvel
 90   | Sabretooth (Victor Creed)    | Bad Characters | Amber Eyes  | Blond Hair  | Male Characters   | 382         | 1977 | marvel
 92   | Johann Shmidt (Earth-616)    | Bad Characters | Blue Eyes   | Blond Hair  | Male Characters   | 376         | 1941 | marvel
 99   | Raven Darkholme (Earth-616)  | Bad Characters | Yellow Eyes | Red Hair    | Female Characters | 371         | 1978 | marvel
 117  | MacDonald Gargan (Earth-616) | Bad Characters | Brown Eyes  | Bald        | Male Characters   | 317         | 1964 | marvel
 118  | Mephisto (Earth-616)         | Bad Characters | White Eyes  | Black Hair  | Male Characters   | 316         | 1968 | marvel
 124  | Thanos (Earth-616)           | Bad Characters | Red Eyes    | No Hair     | Male Characters   | 306         | 1973 | marvel
                                       ^^^^^^^^^^^^^^                                                   ^^^
```


### Сортировка и ограничение количества строк

top-5 злодеек
```sql
SELECT *
FROM superheroes
WHERE align = 'Bad Characters'            -- фильтр
  AND gender = 'Female Characters'        -- из двух условий
ORDER BY appearances DESC
LIMIT 5;
```
здесь фильтр(WHERE) состоит из двух условий,
которые должны выполняться одновременно(AND) тип: плохой И пол женский
- далее идёт сортировка по популярности и ограничение до 5 строк таблицы

```
  id  |              name              |     align      |     eye     |    hair    |      gender       | appearances | year | universe
------+--------------------------------+----------------+-------------+------------+-------------------+-------------+------+----------
   99 | Raven Darkholme (Earth-616)    | Bad Characters | Yellow Eyes | Red Hair   | Female Characters |         371 | 1978 | marvel
  157 | Amora (Earth-616)              | Bad Characters | Green Eyes  | Blond Hair | Female Characters |         247 | 1964 | marvel
 4956 | Talia al Ghul (New Earth)      | Bad Characters | Brown Eyes  | Brown Hair | Female Characters |         177 | 1971 | dc
  221 | Ophelia Sarkissian (Earth-616) | Bad Characters | Green Eyes  | Black Hair | Female Characters |         175 | 1969 | marvel
  229 | Hela (Earth-616)               | Bad Characters | Green Eyes  | Black Hair | Female Characters |         170 | 1964 | marvel
                                         ^^^                                         ^^^^^^                      ^^^ desc
(5 rows)
```



### Сортировка сразу по нескольким столбцам

сортируем по году появления и количеству появлений в комиксах
```sql
SELECT *
FROM superheroes
ORDER BY year, appearances;
```

Здесь сортировка по умолчанию т.е. в порядке возрастания.
```
  id  |             name             |        align       |     eye    |    hair    |       gender      | appearances | year | universe
------+------------------------------+--------------------+------------+------------+-------------------+-------------+------+----------
 5959 | Lady of the Lake (New Earth) | Good Characters    | Blue Eyes  | Blue Hair  | Female Characters | 13          | 1936 | dc
 5352 | Arthur Pendragon (New Earth) | Good Characters    | Brown Eyes | Brown Hair | Male Characters   | 41          | 1936 | dc
 5094 | Merlin (New Earth)           | Neutral Characters | Black Eyes | White Hair | Male Characters   | 92          | 1936 | dc
 6087 | Rose Psychic (New Earth)     | Good Characters    | Black Eyes | Black Hair | Female Characters | 11          | 1937 | dc
 5143 | Cyril Saunders (New Earth)   | Good Characters    | Hazel Eyes | White Hair | Male Characters   | 79          | 1937 | dc
 4941 | Samuel Bradley (New Earth)   | Good Characters    | Grey Eyes  | Grey Hair  | Male Characters   | 213         | 1937 | dc
 5069 | Wing How (New Earth)         | Good Characters    | Brown Eyes | Black Hair | Male Characters   | 103         | 1938 | dc
 4998 | Lee Travis (New Earth)       | Good Characters    | Black Eyes | Black Hair | Male Characters   | 139         | 1938 | dc
 4913 | John Zatara (New Earth)      | Good Characters    | Blue Eyes  | Black Hair | Male Characters   | 256         | 1938 | dc
 4868 | Lois Lane (New Earth)        | Good Characters    | Blue Eyes  | Black Hair | Female Characters | 934         | 1938 | dc
                                                                                                          ^^^ asc       ^^^^
```


### SQL как английский
синтаксис языка SQL достаточно прост и очень напоминает обычный Английский язык.
- запросы составляются почти так же как если бы мы говорили на англ. языке.
  вот пример запроса позволяющего получить топ-5 злодеек:

```sql
SELECT *                           -- извлеч всё
FROM superheroes                   -- из таблицы супергероев
WHERE align = 'Bad Characters'     -- где сторона - плохие персонажи
  AND gender = 'Female Characters' --          и  пол женский
ORDER BY appearances DESC          -- отсортируй в порядке убывания по количеству
                                   -- появлений
LIMIT 5                            -- огранич результат 5 строками.
```

Изначально язык назывался SEQUEL(Structured English QUEry Language)
Структурированный англ. язык запросов. Позже из названия языка убрали слово
English но логика осталась той же. Простые запросы можно понять просто если
знать англ. язык. Но так же комбинации операторов позволяют строить громоздкие
и сложные предложения которые понять будет уже не так просто.


