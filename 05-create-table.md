- Работа с таблицами в SQL
  Создание таблицы – CREATE TABLE
  Удаление таблицы – DROP TABLE
  Изменение таблицы – ALTER TABLE

- Языки в SQL
  Data Definition Language (DDL) – язык описания данных

  Data Manipulation Language (DML) – язык манипулирования данными
  содержит такие операторы как SELECT, INSERT, UPDATE, DELETE

  Data Control Language (DСL) – язык управления доступом к данных
  позволяет описать права доступа пользователей к данным. кому куда можно и
  кому нельзя.

- Работа с таблицами отличается в разных реализациях СУБД
  Смотрите документацию по используемой системе управления базами данных(СУБД)

## Создание таблиц

Как в SQL создвать таблицы.

В реляционной Базе Данных таблица:
- имеет название  ( в нашем примере это была таблица superheroes)
- столбцы их еще называют атрибуты
  в таблице из прошлых примеров это:
  id, name, align, eye, hair, gender, appearances, year, universe
- каждый столбец имеет своё имя - название столбца


CREATE TABLE - оператор для создания таблицы.


```sql
CREATE TABLE superheroes(
    id INT,                  -- идентификатор целочисленный тип
    name VARCHAR(100),       -- имя персонажа строка -  макс 100 символов.
    align VARCHAR(30),
    eye VARCHAR(30),
    hair VARCHAR(30),
    gender VARCHAR(30),
    appearances INT,         -- количество появлений в комиксах(целое число)
    year INT,                -- год появления (целое число)
    universe VARCHAR(10)
);
```
синтаксис создания таблицы:
 CREATE TABLE <имя таблицы>(список-столбцов);

каждый столбец внутри списка столбцов включает в себя:
- название(имя) стобца
  в примере выше имена столбцов:
  id, name, align, eye, hair, gender, appearances year, universe
- тип хранимого значения
  - INT - целое число
  - VARCHAR - строка переменной длинны
  и другие типы.(в нашей есть пока только числа и строки)



### Типы данных используемые в SQL


  Тип Данных               Название
- CHARACTER(n) (CHAR(n))   Строка фиксированной длины n (БД хранит все n символов)
- CHARACTER VARYING(n)     Строка переменной длины, максимальная длина n
            VARCHAR(n)     (используется чаще чем CHAR, хранит только n символов)
- BOOLEAN                  Логический тип данных
- INTEGER (INT)            Целое число
- NUMERIC(p,s)             Действительное число (p – количество значащих цифр,
                           s – количество цифр после запятой). Хранится точно.
- REAL                     Действительное число одинарной точности,
                           формат IEEE 754
- DOUBLE PRECISION         Действительное число двойной точности, формат IEEE 754

- DATE                     Дата
- TIMESTAMP                Дата и время

Это только часть наиболее часто используемых типов. на деле их больше.
Каждая СУБД в дополнение к стандартным типом может использовать еще и свои.

REAL DOUBLE - это числа с плавающей точкой стандарт IEEE 754. Операции с такими
числами работают быстрее чем с NUMERIC. но этот формат не годится для хранения
денег(проблема потери точности) Для хранения денег используют более точный но
чуть медленный NUMERIC


### Первичный ключ в таблице
Для облегчения работы с данными обычно в каждой таблице создают
первичный(основной) ключ Primary Key.
Основной(первичный) ключ - нужен для возможности уникально идентифицировать
записи в таблице. т.е. для возможности отличать их друг от друга.


```sql
CREATE TABLE superheroes(
    id INT PRIMARY KEY,       -- первичный ключ таблицы superheroes
    name VARCHAR(100),
    align VARCHAR(30),
    eye VARCHAR(30),
    hair VARCHAR(30),
    gender VARCHAR(30),
    appearances INT,
    year INT,
    universe VARCHAR(10)
);
```

`id INT PRIMARY KEY` говорит о том, что в этой таблице в колонке `id` значения
должны быть строго уникальны. т.е. не должны повторятся в рамках данной таблицы.
Другими словами две записи(строки таблицы(row)) не могут иметь одно и тоже
значение в поле(колонке) `id`. То есть значения в этом поле должны быть строго
уникальны.



### Автоматическое заполнение первичного ключа.

Создание первичных ключей - сложная задача, например нужно знать какое значение
еще не использовалось, чтобы не создать дублирующиеся значения.
Многие СУБД имеют встроенные средства для автоматизации этой задачи.
Т.е. позволяют автоматически при вставке заполнять поле(колонку) новым
уникальным значением.

`SERIAL` - ключевое слово(тип) в PostgeSQL, используемое для автогенерации
уникальных целочисленных идентификаторов.

```sql
CREATE TABLE superheroes(
    id SERIAL PRIMARY KEY,       -- автоматическое создание ID
    name VARCHAR(100),
    align VARCHAR(30),
    eye VARCHAR(30),
    hair VARCHAR(30),
    gender VARCHAR(30),
    appearances INT,
    year INT,
    universe VARCHAR(10)
);
```

Это работает довольно просто. При вставке новой строки в таблицу будет
автоматически подставляться следующее еще не используемое значение с авто
инкрементом.

SERIAL НЕ является стандартным для SQL. В других СУБД для подобных вещей
используются другие ключевые слова. Вот примеры:
MySQL: AUTO_INCREMENT


### Смотрим сведенья о созданной таблице

входим в консольный клиент psql
```sh
sudo su postgres
psql
# psql (13.14 (Debian 13.14-0+deb11u1))
```
через встроенную команду оболочки psql `\d <имя таблицы>`:
```
postgres=# \d superheroes;
                                   Table "public.superheroes"
   Column    |          Type          | Collation | Nullable |             Default
-------------+------------------------+-----------+----------+----------------------------------
 id          | integer                |           | not null | generated by default as identity
 name        | character varying(100) |           |          |
 align       | character varying(30)  |           |          |
 eye         | character varying(30)  |           |          |
 hair        | character varying(30)  |           |          |
 gender      | character varying(30)  |           |          |
 appearances | integer                |           |          |
 year        | integer                |           |          |
 universe    | character varying(10)  |           |          |
Indexes:
    "superheroes_pkey" PRIMARY KEY, btree (id)
```

Обрати внимание что PostgeSQL пишет полное название типа а не сокращенное.
integer а не INT как в команде создания таблицы.
Nullable - допустимость NULL-значения.
not null - означает что значение в этом столбце не может быть NULL(т.е. должно
быть обязательно задано)
Default - значение по умолчанию. Здесь показано какое значение будет подставлять
СУБД если оно не задано при вставке.
здесь в примере для id это `generated by default as identity`
у лектора в слайдах nextval('superheroes_id_seq'::regclass)

Из этого вывода еще видно что для поля id создан индекс:
```
Indexes:
    "superheroes_pkey" PRIMARY KEY, btree (id)
```
Об индексах и зачем они вообще нужны будет далее по курсу.

Команды для отображения инфы о таблицах отличаются в разных СУБД.
- Oracle MySQL: `describe superheroes` или сокращённо `desc`


### Удаление таблицы DROP TABLE name

DROP - "выбрасить"
```sql
DROP TABLE superheroes;
```


```sql
-- Создание таблиц
-- Создаем таблицу супергероев
DROP TABLE IF EXISTS superheroes; -- удалить таблицу если существует
CREATE TABLE superheroes(
    id INT PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY,
    name VARCHAR(100),
    align VARCHAR(30),
    eye VARCHAR(30),
    hair VARCHAR(30),
    gender VARCHAR(30),
    appearances INT,
    year INT,
    universe VARCHAR(10)
);
```

`DROP TABLE IF EXISTS` - удалит таблицу только в случае если она уже есть в БД.
для повторного запуска скрипта.



### Изменение таблиц в SQL
ALTER - изменить

ALTER TABLE <table_name> <ACTION>;
```sql
-- изменить таблицу добавив в венё новый столбец "жив-или-нет"
ALTER TABLE superheroes ADD COLUMN alive BOOLEAN;

ALTER TABLE superheroes
  ADD COLUMN first_appearance TIMESTAMP;

-- пример удаления столбца
ALTER TABLE superheroes DROP COLUMN year;

-- переименование столбца из name в hero_name
ALTER TABLE superheroes RENAME COLUMN name TO hero_name;

-- задать всей таблице новое имя вместо старого.
ALTER TABLE superheroes RENAME TO comic_characters;
```
это только часть того что можно менять.
смотри документацию к СУБД.
