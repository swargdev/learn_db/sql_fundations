- Вставка данных в таблицы
  Оператор `INSERT`

- Изменение данных в таблице
  Оператор `UPDATE`

- Удаление данных из таблиц
  Оператор `DELETE`

- Особенности всех вышеописанных операторов
  Один оператор может менять несколько строк данных
  Фильтры в `WHERE` такие же, как в `SELECT`
  Первичный ключ позволяет однозначно идентифицировать строки
  что будет гарантировать корректность операции.


## Вставка и изменение данных в SQL

Команда для создания таблицы
```sql
CREATE TABLE superheroes(
  id SERIAL PRIMARY KEY,
  name VARCHAR(100),
  align VARCHAR(30),
  eye VARCHAR(30),
  hair VARCHAR(30),
  gender VARCHAR(30),
  appearances INT,
  year INT,
  universe VARCHAR(10)
);
```

`INSERT` - оператор для вставки данных(строки) в заранее созданную таблицу.

пример вставки в заранее созданную таблицу
```sql
INSERT INTO superheroes(name, appearances, universe)
VALUES ('Spider-Man', 4043, 'marvel');
```

```sql
INSERT INTO <имя таблицы>(<названия столбцов>)
VALUES (<значения соответствующие столбцам>)
```

```sql
INSERT INTO person(name, appearances, universe)
VALUES ('Spider-Man', 4043, 'marvel');

-- INSERT 0 1
-- Time: 21.794 ms

SELECT * FROM person;
--  id |    name    | align | eye | hair | gender | appearances | year | universe
-- ----+------------+-------+-----+------+--------+-------------+------+----------
--   1 | Spider-Man |       |     |      |        |        4043 |      | marvel
-- (1 row)
```
обрати внимание значение для поля id вставилось автоматически.

Пример того как вставить сразу все данные
```sql
INSERT INTO superheroes(name, align, eye, hair, gender, appearances, year, universe)
VALUES ('Spider-Man (Peter Parker)', 'Good Characters', 'Hazel Eyes',
        'Brown Hair', 'Male Characters', 4043, 1962, 'marvel')
```

Если нужно можно явно вручную задавать значение ID даже несмотря на то что он
прописан автогенерируемый:
```sql
INSERT INTO superheroes(id, name, align, eye, hair, gender, appearances, year, universe)
VALUES (1, 'Spider-Man (Peter Parker)', 'Good Characters', 'Hazel Eyes',
        'Brown Hair', 'Male Characters', 4043, 1962, 'marvel')
```

Есть и такая "упрощённая" форма вставки данных(без явного указания списка полей)
крайне не рекомендуется использовать подобный синтаксис - высок шанс ошибки.
Если со временем колонки в таблице изменится то такая вставка перестанет работать.
```sql
INSERT INTO superheroes
VALUES (1, 'Spider-Man (Peter Parker)', 'Good Characters', 'Hazel Eyes',
        'Brown Hair', 'Male Characters', 4043, 1962, 'marvel')
```


### Изменение данных в таблице

UPDATE - изменить(обновить) данные в таблице

UPDATE <имя-таблицы> SET <имена и значения столбцов> WHERE <условие>
самое важное - наличие условия для WHERE, описывающее где именно делать
изменения.

```sql
UPDATE superheroes
SET name='Batman',
    universe='dc'
WHERE id=1;
```
в этом примере запись(row) таблицы с ID равным 1 мы меняем значение колонки
(поля) name и universe на заданные

Изменение для нескольких строк-записей(row) в таблице по заданному фильтру

измненяем слишком длинное название пола на более короткое
```sql
UPDATE superheroes
SET gender='Man'
WHERE gender='Male Characters';
```
этот запрос изменит значения поля(колонки) gender для всех записей у которых
до этого оно было равно `Male Characters`


```
 id |            name            |        align       |     eye    |    hair    | gender | appearances | year | universe
----+----------------------------+--------------------+------------+------------+--------+-------------+------+----------
 5  | Thor (Thor Odinson)        | Good Characters    | Blue Eyes  | Blond Hair | Man    | 2258        | 1950 | marvel
 6  | Benjamin Grimm (Earth-616) | Good Characters    | Blue Eyes  | No Hair    | Man    | 2255        | 1961 | marvel
 7  | Reed Richards (Earth-616)  | Good Characters    | Brown Eyes | Brown Hair | Man    | 2072        | 1961 | marvel
 8  | Hulk (Robert Bruce Banner) | Good Characters    | Brown Eyes | Brown Hair | Man    | 2017        | 1962 | marvel
 9  | Scott Summers (Earth-616)  | Neutral Characters | Brown Eyes | Brown Hair | Man    | 1955        | 1963 | marvel
 10 | Jonathan Storm (Earth-616) | Good Characters    | Blue Eyes  | Blond Hair | Man    | 1934        | 1961 | marvel
                                                                                  ^^^
```

### Удаление данных из таблицы

Пример удаления одной строки по ID
```sql
DELETE FROM superheroes
WHERE id=2;
```

Пример удаления сразу нескольких строк через фильтр.(удаление всех строк
подходящих под заданный фильтр)
```sql
DELETE FROM superheroes
WHERE gender='Male Characters';
```

удаление всех данных из таблицы - просто не указывай условие(т.е. без WHERE)
```sql
DELETE FROM superheroes;
```

Полезность первичного ключа уже вырисовывается при изменении и удалении строк
таблицы. Первичный(Primary) ключ позволяет точно и однозначно идентифицировать
т.е. указывать СУБД с какой конкретно строкой мы хотим сейчас работать. А
уникальность нам гарантирует что такая строка будет только одна. Если бы это
было не так, то это бы вносило неконсистентность (ломало бы логическую
целостность данных) при выполнении операций изменяющих хранимые данные.
Например если бы мы по ID обращались бы к строке, а таких строк с там же ID
было бы несколько то при UPDATE или DELETE операция бы применялась ко всем им,
а не к одной, которую нам нужно было поменять. А это явно не то что надо.

