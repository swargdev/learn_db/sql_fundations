#!/bin/bash

DIR=$(dirname "$0")
DB_NAME="db_sql_foundation_sozikin"

## psql -d <db_name> -f <script.sql>
## workaround to run scipt from another user
function run_sql_script() {
  local path="${DIR}/${1}"
  local subpath="${1//\//_}"
  if [ -f "$path" ]; then
    cp "$path" "/tmp/$subpath"
    sudo -i PWD=/tmp -H -u postgres psql -d ${DB_NAME} -f "/tmp/$subpath"
  fi
}

function create_db() {
  sudo -i PWD=/tmp -H -u postgres psql -c "CREATE DATABASE $DB_NAME;"
}

function drop_db() {
  sudo -i PWD=/tmp -H -u postgres psql -c "DROP DATABASE $DB_NAME;"
}

if [[ $1 == 'new' ]]; then
  # scheme + insert
  create_db
  run_sql_script "sql_foundation.sql"

elif [[ $1 == 'drop' ]]; then
  drop_db

else
  echo "new|drop"
fi

