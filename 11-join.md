Здесь рассматриваем т.н. "внутренее" объединение данных в таблицах.

- Запрос данных их нескольких таблиц
  - Оператор `SELECT`
  - В ключевом слове `FROM` указываем несколько таблиц с `JOIN`
  - После ключевого слова `ON` указываем условия объединения таблиц

- Связи между таблицами
  (чтобы извлекать данные между таблицами они должны быть связаны FK->PK)
  - Ссылки из одной таблицы на другую
  - Внешний ключ (`FOREIGN KEY`) FK используется в условия для `ON`

- Использование `JOIN` в `SELECT`
  - Комбинация с `WHERE`, `ORDER BY`, `LIMIT`, `GROUP BY`, `HAVING`
  - Типы `JOIN`: внешнее и внутреннее, левое и правое, перекрестное, полное
    (тема следующего занятия здесь будет только про внутренее обьединение)



## Запрос данных из нескольких таблиц

в прошлом занятии было о том как выполнять декомпозицию данных то есть
размещать в нескольких таблицах базы данных. Здесь будет о том, как извлекать
данные из нескольких таблиц через оператор `SELECT`.

В этих и последующих примерах будем использовать бд онлайн-школы.
В этой бд есть несколько сущностей, размещённых в нескольких таблицах одной БД.
Это отлично подходит для изучения работы с соединением(Join) таблиц.

Здесь рассмотрим две таблицы `products` и `product_types`

`products`       (id, name, type_id, price)
```
 id |                             name                    | type_id | price
----+-----------------------------------------------------+---------+-------
  1 | Основы искусственного интеллекта                    |       1 | 15000
  2 | Технологии обработки больших данных                 |       1 | 50000
  3 | Программирование глубоких нейронных сетей           |       1 | 30000
  4 | Нейронные сети для анализа текстов                  |       1 | 50000
  5 | Нейронные сети для анализа изображений              |       1 | 50000
  6 | Инженерия искусственного интеллекта                 |       1 | 60000
  7 | Как стать DataScientist'ом                          |       2 |     0
  8 | Планирование карьеры в DataScience                  |       2 |  2000
  9 | Области применения нейросетей:                      |       2 |  4000
 10 | Программирование глубоких нейронных сетей на Python |       3 |  1000
 11 | Математика для DataScience                          |       3 |  2000
 12 | Основы визуализации данных                          |       3 |   500
```

`products_types` (id, type_name)
```
 id |  type_name
----+--------------
  1 | Онлайн-курс
  2 | Вебинар
  3 | Книга
  4 | Консультация
```

products.type_id --> product_types.id  связь между таблицами(FK->PK)
Пример
"Основы искусственного интеллекта" type_id=1  -->  "Онлайн-курс"
"Как стать DataScientist'ом"       type_id=2  -->  "Вебинар"
 "Математика для DataScience"      type_id=3  -->  "Книга"



## Объединение данных из нескольких таблиц в SELECT

JOIN - ключевое слово для объединения нескольких таблиц
```sql
SELECT products.name, product_types.type_name
FROM products JOIN product_types               -- обьединение двух таблиц
ON products.type_id = product_types.id;        -- условие обьединения(JOIN)
```
- здесь идёт извлечение(выборка) данных из обьеденённой таблицы получившейся
в ходе обьединения двух таблиц products и product_types
- после ОN указывается условия обьединения двух таблиц - здесь это по из ID

так как здесь работает сразу с двумя таблицами, то для корректной работы
нам нужно точно определять откуда брать колонки. И здесь это делаем через:
<имя таблицы>.<имя столбца>
например products.type_id, product_types.id

- `ON` products.type_id = product_types.id
говорит о том что значение в колонке type_id таблицы products должно быть
равно со значению в колонке id таблицы product_types.

- `SELECT products.name, product_types.type_name`
в первой части оператора SELECT мы указываем какие именно столбцы нас интересуют
то есть те столбцы и их значения которые СУБД нам отдаст в результате обьединения

Результатом будет такая таблица:
```
                             name                              |  type_name
---------------------------------------------------------------+-------------
 Основы искусственного интеллекта                              | Онлайн-курс
 Технологии обработки больших данных                           | Онлайн-курс
 Программирование глубоких нейронных сетей                     | Онлайн-курс
 Нейронные сети для анализа текстов                            | Онлайн-курс
 Нейронные сети для анализа изображений                        | Онлайн-курс
 Инженерия искусственного интеллекта                           | Онлайн-курс
 Как стать DataScientist'ом                                    | Вебинар
 Планирование карьеры в DataScience                            | Вебинар
 Области применения нейросетей: в какой развивать экспертность | Вебинар
 Программирование глубоких нейронных сетей на Python           | Книга
 Математика для DataScience                                    | Книга
 Основы визуализации данных                                    | Книга
(12 rows)
```


## Способ написания запроса обьединения таблиц в более короткой форме - алиасы

Псевдонимы позволяют сильно уменьшить объём запроса на объединение таблиц
```sql
SELECT p.name, t.type_name
FROM products AS p JOIN product_types AS t -- указываем какие именно таблицы
                                           -- соединять и их псевдонимы.
ON p.type_id = t.id
```

### Псевдонимы столбцов и таблиц

```sql
SELECT p.name AS product_name,
       t.type_name AS product_type,
       p.price AS product_price
FROM products AS p JOIN product_types AS t
ON p.type_id = t.id;
```


### Фильтрация данных из нескольких таблиц
В запросах данных из нескольких таблиц можно использовать все ранее изученные
возможности оператора SELECT. Например можно делать фильтрацию (WHERE):
```sql
SELECT p.name AS product_name,
       t.type_name AS product_type,
       p.price AS product_price
FROM products AS p JOIN product_types AS t
ON p.type_id = t.id
WHERE t.type_name='Онлайн-курс';
```
```
               product_name                | product_type | product_price
-------------------------------------------+--------------+---------------
 Основы искусственного интеллекта          | Онлайн-курс  |         15000
 Технологии обработки больших данных       | Онлайн-курс  |         50000
 Программирование глубоких нейронных сетей | Онлайн-курс  |         30000
 Нейронные сети для анализа текстов        | Онлайн-курс  |         50000
 Нейронные сети для анализа изображений    | Онлайн-курс  |         50000
 Инженерия искусственного интеллекта       | Онлайн-курс  |         60000
```

## Пример фильтрации данных из нескольких таблиц

найти все безплатные вебинары

```sql
SELECT p.name AS product_name,
       t.type_name AS product_type,
       p.price AS product_price
FROM products AS p JOIN product_types AS t
ON p.type_id = t.id
WHERE t.type_name = 'Вебинар'
  AND p.price = 0;                            -- второе условие цена продукта
```

```
        product_name        | product_type | product_price
----------------------------+--------------+---------------
 Как стать DataScientist'ом | Вебинар      |             0
```


## Сортировка данных из нескольких таблиц
```sql
SELECT p.name AS product_name,
       t.type_name AS product_type,
       p.price AS product_price
FROM products AS p JOIN product_types AS t
  ON p.type_id = t.id
WHERE t.type_name='Онлайн-курс'
ORDER BY p.price DESC;               -- указание столбца по которому сортировать
--                ^сортировать по убыванию
--       ^ ^-название столбца
--       псевдоним таблицы
```

```
               product_name                | product_type | product_price
-------------------------------------------+--------------+---------------
 Инженерия искусственного интеллекта       | Онлайн-курс  |         60000
 Технологии обработки больших данных       | Онлайн-курс  |         50000
 Нейронные сети для анализа текстов        | Онлайн-курс  |         50000
 Нейронные сети для анализа изображений    | Онлайн-курс  |         50000
 Программирование глубоких нейронных сетей | Онлайн-курс  |         30000
 Основы искусственного интеллекта          | Онлайн-курс  |         15000
```
