- Ключевое слово WHERE
  фильтрация сток, с которыми будут выполняться действия
  используется не только в `SELECT`, но и в других операторах SQL
  здесь пока примеры только с SELECT

- Операторы сравнения
  операторы которые можно использовать "внутри" WHERE:
  =, !=, <>, >, >=, <, <=
  IN, BETWEEN, LIKE

- Логические операции
  AND, OR, NOT


## Фильтрация данных в SQL: WHERE


Извлечь два столбца name и appearances из таблицы superheroes
```sql
SELECT name, appearances
FROM superheroes;
```

Для того чтобы указать какие именно строки нужно выбрать из таблицы нужно
использовать "фильтр" - ключ.слово `WHERE`(Где)


```sql
SELECT *
FROM superheroes
WHERE gender = 'Female Characters';
```

Здесь в последней строке описываем фильтр позволяющий выбрать только женские
персонажи:
```
WHERE <название-столбца> = <конкретное-значение>
```

Output:
```
id|            name              |       align     |       eye  |     hair   |      gender       | appearances | year | universe
--+------------------------------+-----------------+------------+------------+-------------------+-------------+------+---------
12| Susan Storm (Earth-616)      | Good Characters | Blue Eyes  | Blond Hair | Female Characters |        1713 | 1961 | marvel
13| Ororo Munroe (Earth-616)     | Good Characters | Blue Eyes  | White Hair | Female Characters |        1512 | 1975 | marvel
17| Mary Jane Watson (Earth-616) | Good Characters | Green Eyes | Red Hair   | Female Characters |        1304 | 1965 | marvel
24| Wanda Maximoff (Earth-616)   | Good Characters | Green Eyes | Brown Hair | Female Characters |        1161 | 1964 | marvel
```

Получаем тип персонажей со стороной(align) "перевоспитанные преступники":
```sql
SELECT *
FROM superheroes
WHERE align = 'Reformed Criminals';
```

Output:
```
  id  |             name              |       align        |    eye    |    hair    |      gender       | appearances | year | universe
------+-------------------------------+--------------------+-----------+------------+-------------------+-------------+------+----------
 5144 | Owen Mercer (New Earth)       | Reformed Criminals | Blue Eyes | Red Hair   | Male Characters   |          78 | 2004 | dc
 6550 | Paula von Gunther (New Earth) | Reformed Criminals | Blue Eyes | Blond Hair | Female Characters |           5 | 1998 | dc
                                        ^^^^^^^^^^^^^^^^^^
```

### Операторы сравнения в WHERE

Оператор   Назначение
- =          равно
- <> или !=  неравно
- >          больше
- >=         больше или равно
- <          меньше
- <=         меньше или равно
- between    значение внутри указанного диапазона
- in         значение входит в список
- like       проверка строки(String) на соответствие шаблону


### Выборка персонажей появившихся между(BETWEEN) 2000 и 2005 годами
```sql
SELECT *
FROM superheroes
WHERE year BETWEEN 2000 AND 2005;
```

```
  id  |             name            |        align       |     eye    |    hair    |       gender      | appearances | year | universe
------+-----------------------------+--------------------+------------+------------+-------------------+-------------+------+----------
 111  | Maria Hill (Earth-616)      | Good Characters    | Brown Eyes | Black Hair | Female Characters | 325         | 2005 | marvel
 140  | Laura Kinney (Earth-616)    | Good Characters    | Green Eyes | Black Hair | Female Characters | 265         | 2004 | marvel
 164  | Robert Reynolds (Earth-616) | Neutral Characters | Blue Eyes  | Blond Hair | Male Characters   | 240         | 2000 | marvel
 ...
 7158 | Rosemary Fields (New Earth) | Good Characters    | Blue Eyes  | Red Hair   | Female Characters | 1           | 2000 | dc
(977 rows)
```

### использование IN в WHERE

Здесь мы ищем всех персонажей с рыжим цветов волос(три оттенка заданных списком)
```sql
SELECT *
FROM superheroes
WHERE hair IN ('Strawberry Blond Hair', 'Red Hair', 'Auburn Hair');
```

```
 id |             name             |      align      |     eye    |   hair   |       gender      | appearances | year | universe
----+------------------------------+-----------------+------------+----------+-------------------+-------------+------+----------
 15 | Matthew Murdock (Earth-616)  | Good Characters | Blue Eyes  | Red Hair | Male Characters   | 1338        | 1964 | marvel
 17 | Mary Jane Watson (Earth-616) | Good Characters | Green Eyes | Red Hair | Female Characters | 1304        | 1965 | marvel
...
583 rows...
```

### использование LIKE в WHERE
LIKE - используется для сравнения (текстовых)строк (String не Row(строк таблицы))
используя шаблоны. Шаблоны простейшие и имеют только два спец символа:

Специальные символы в шаблонах `LIKE`
- `%` - любое количество символов (включая 0)
- `_` - ровно один символ

Пример: получение всех персонажей у которых цвет волос включает `Blond`
```sql
SELECT *
FROM superheroes
WHERE hair LIKE '%Blond%';
```

Output: 1154 rows:
```
 id |                name               |      align      |     eye    |          hair         |       gender      | appearances | year | universe
----+-----------------------------------+-----------------+------------+-----------------------+-------------------+-------------+------+----------
 5  | Thor (Thor Odinson)               | Good Characters | Blue Eyes  | Blond Hair            | Male Characters   | 2258        | 1950 | marvel
...
 47 | Eugene Thompson (Earth-616)       | Good Characters | Blue Eyes  | Strawberry Blond Hair | Male Characters   | 689         | 1962 | marvel
...
 79 | Johnathon Blaze (Earth-616)       | Good Characters | Blue Eyes  | Reddish Blond Hair    | Male Characters   | 426         | 1972 | marvel
                                                                                 ^^^^^
```

### Логически операции в WHERE
Такие же логические операции как во многих Языках Программирования(ЯП)

- `AND` логическое И
- `OR`  логического ИЛИ
- `NOT` логическое НЕ


#### WHERE + AND

Получить всех "плохих" девушек - два поля пол(`gender`) и тип персонажа(`align`)
```sql
SELECT *
FROM superheroes
WHERE gender = 'Female Characters'
      AND
      align = 'Bad Characters';
```

```
  id |              name              |      align     |     eye     |    hair    |       gender      | appearances | year | universe
-----+--------------------------------+----------------+-------------+------------+-------------------+-------------+------+----------
 99  | Raven Darkholme (Earth-616)    | Bad Characters | Yellow Eyes | Red Hair   | Female Characters | 371         | 1978 | marvel
 157 | Amora (Earth-616)              | Bad Characters | Green Eyes  | Blond Hair | Female Characters | 247         | 1964 | marvel
 221 | Ophelia Sarkissian (Earth-616) | Bad Characters | Green Eyes  | Black Hair | Female Characters | 175         | 1969 | marvel
...
```


### WHERE + OR

Получить всех персонажей с рыжим цветом волос(все оттенки)
```sql
SELECT *
FROM superheroes
WHERE hair = 'Red Hair'
  OR hair = 'Strawberry Blond Hair'
  OR hair = 'Auburn Hair';
```
такой фильтр пропускает строчки(rows) с персонажами у которых цвет волос
равен либо 1му либо 2му либо 3му названию цвета.

```
 id |             name             |      align      |     eye    |          hair         |       gender      | appearances | year | universe
----+------------------------------+-----------------+------------+-----------------------+-------------------+-------------+------+----------
 15 | Matthew Murdock (Earth-616)  | Good Characters | Blue Eyes  | Red Hair              | Male Characters   | 1338        | 1964 | marvel
 ...
 26 | Janet van Dyne (Earth-616)   | Good Characters | Blue Eyes  | Auburn Hair           | Female Characters | 1120        | 1963 | marvel
 ...
 47 | Eugene Thompson (Earth-616)  | Good Characters | Blue Eyes  | Strawberry Blond Hair | Male Characters   | 689         | 1962 | marvel
```


###  WHERE + NOT

NOT можно использовать не только в комбинации выражений с AND OR но и совместно
с BETWEEN, IN, LIKE:

Поиск персонажей с редким цветов волос.
Составляем список часто встречающихся цвет волос и запрашиваем всех у кого
цвета волос НЕ входят в этот список
```sql
SELECT *
FROM superheroes
WHERE hair NOT IN ('Blond Hair', 'Black Hair', 'Brown Hair', 'Red Hair');
```

```
 id |               name              |      align      |    eye    |    hair    |      gender     | appearances | year | universe
----+---------------------------------+-----------------+-----------+------------+-----------------+-------------+------+----------
 2  | Captain America (Steven Rogers) | Good Characters | Blue Eyes | White Hair | Male Characters | 3360        | 1941 | marvel
 6  | Benjamin Grimm (Earth-616)      | Good Characters | Blue Eyes | No Hair    | Male Characters | 2255        | 1961 | marvel
 11 | Henry McCoy (Earth-616)         | Good Characters | Blue Eyes | Blue Hair  | Male Characters | 1825        | 1963 | marvel
...
```


