
- Оператор `SELECT`
  извлечение данных из таблиц реляционной базы данных(СУБД)

- Ключевое слово `FROM`
  Источник данных

- Ключевое слово `DISTINCT`
  выбор уникальных значений в столбцах

- Ключевое слово `LIMIT`
  Ограничение количества извлекаемых строк

- Ключевое слово `AS`
  создание псевдонима


## Оператор SELECT
Оператор выборки данных из таблиц. Откуда именно извлекать данные указывается
через ключевое слово `FROM`. Для получения(выборки) только уникальных значений
из столбца используется ключ. слово DISTINCT. Для ограничения выборки - LIMIT
AS - позволяет создавать псевдонимы столбцов.

### Выборка всех строк из заданной таблицы

Получить список всех супер-героев из заданной таблицы,
показав все имеющиеся столбцы

```sql
select * from superheroes;
```

Получить список всех, но только с двумя столбцами - имя и кол-во появлений.
```sql
select name, appearances from superheroes;
```
```
                name                     | appearances
-----------------------------------------+-------------
 Spider-Man (Peter Parker)               |        4043
 Captain America (Steven Rogers)         |        3360
 Wolverine (James \"Logan\" Howlett)     |        3061
 Iron Man (Anthony \"Tony\" Stark)       |        2961
```


### Псевдонимы столбцов в SELECT
здесь мы задаём свой псевдоним столбца при отображении (вместо name -> hero_hame)
Польза псевдонимов при выборки в сложных запросов, когда выбираются некие
выражения либо при использовании в функциях

```sql
SELECT name AS hero_name,
       appearances
FROM superheroes;
```

```
                               hero_name                      | appearances
--------------------------------------------------------------+-------------
 Spider-Man (Peter Parker)                                    |        4043
 Captain America (Steven Rogers)                              |        3360
```

Можно опускать ключевое слово `AS` и просто писать псевдоним сразу после
названия столбца:
```sql
SELECT name hero_name,
       appearances
FROM superheroes;
```
(Это менее понятно и читается хуже, но можно и так)


### Выбор уникальных значений столбцов в SELECT
когда нужно получить только уникальные значения из конктерного столбца таблицы
DISTINCT - "различные" в скобках название столбца из которого нужно получить
уникальное значение.(здесь это align)

```sql
SELECT DISTINCT(align)
FROM superheroes;
```

результат
```
       align
--------------------
 Good Characters
 Neutral Characters
 Reformed Criminals
 Bad Characters
(4 rows)
```

```sql
SELECT DISTINCT(eye)
FROM superheroes;
```
output:
```
        eye
--------------------
 Black Eyeballs
 One Eye
 Purple Eyes
 Orange Eyes
 ...
 Photocellular Eyes
(24 rows)
```

### LIMIT получить не более заданного количества строк таблицы
Как получить скажем только первых 10 результатов выборки, а не всё что есть.

```sql
SELECT DISTINCT(hair)
FROM superheroes
LIMIT 10;
```

Этим запросом:
- извлекаем уникальные(DISTINCT) значения из столбца `hair`(цвет волос)
- из таблицы(FROM) `superheroes`
- ограничивая выборку первыми 10 строками

Output:
```
         hair
-----------------------
 Strawberry Blond Hair
 Violet Hair
 Green Hair
 Bald
 Silver Hair
 Light Brown Hair
 Red Hair
 White Hair
 Purple Hair
 Reddish Blond Hair
(10 rows)
```


SQL - Декларативный язык
на нём пишем что хотим получить, но не как именно это делать.
За конкретику извлечения отвечает конкретная СУБД обрабатывающая наш запрос.
В этом курсе не будет о том, как именно это работает под капотом.
