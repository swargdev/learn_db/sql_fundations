https://leetcode.com/problems/database
https://leetcode.com/problems/combine-two-tables/description/

ddl+dml schema+data_insertion(from the task conditions)
```sql
CREATE TABLE IF NOT EXISTS Person (
  personId int, firstName varchar(255), lastName varchar(255)
);
CREATE TABLE IF NOT EXISTS Address (
  addressId int, personId int, city varchar(255), state varchar(255)
);
TRUNCATE TABLE Person;
INSERT INTO Person (personId, lastName, firstName) VALUES ('1', 'Wang', 'Allen');
INSERT INTO Person (personId, lastName, firstName) VALUES ('2', 'Alice', 'Bob');

TRUNCATE TABLE Address;
INSERT INTO Address (addressId, personId, city, state) VALUES ('1', '2', 'New York City', 'New York');
INSERT INTO Address (addressId, personId, city, state) VALUES ('2', '3', 'Leetcode', 'California');
```

clear tmp-db
```sql
DROP TABLE Person, Address;
```


### Table: Person

```
 Column Name | Type
-------------+---------
 personId    | int
 lastName    | varchar
 firstName   | varchar
```
`personId` is the primary key (column with unique values) for this table.
This table contains information about the ID of some persons and
their first and last names.


### Table: Address

```
 Column Name | Type
-------------+---------
 addressId   | int
 personId    | int
 city        | varchar
 state       | varchar
```
`addressId` is the primary key (column with unique values) for this table.
Each row of this table contains information about the city and state of one
person with `ID = PersonId`.


### Task:

Write a solution to report the `first name`, `last name`, `city`, and `state` of
each person in the `Person` table.
If the address of a `personId` is not present in the `Address` table,
report null instead.

Return the result table in any order.

The result format is in the following example.

### Example 1:

Input:
Person table:
```
 personId | lastName | firstName
----------+----------+-----------
 1        | Wang     | Allen
 2        | Alice    | Bob
```
Address table:
```
 addressId | personId | city          | state
-----------+----------+---------------+------------
 1         | 2        | New York City | New York
 2         | 3        | Leetcode      | California
```
(Expected Solution) Output:
```
 firstName | lastName | city          | state
-----------+----------+---------------+----------
 Allen     | Wang     | Null          | Null
 Bob       | Alice    | New York City | New York
```


### Explanation:

There is no address in the `Address` table for the `personId = 1` so
we return `null` in their city and state.
addressId = 1 contains information about the address of personId = 2.



## My Solution

```sql
SELECT p.firstName, p.lastName, a.city, a.state
FROM person AS p LEFT OUTER JOIN address AS a
  ON p.personId = a.personId
;
```

Output:
```
 firstname | lastname |     city      |  state
-----------+----------+---------------+----------
 Allen     | Wang     |               |
 Bob       | Alice    | New York City | New York
(2 rows)
```
