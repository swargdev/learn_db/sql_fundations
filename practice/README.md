## Практика SQL

Здесь собраны описания и разбор практических задач.
Все задачи проверяю на живой базе данных PostgreSQL.
в условии задачи есть возможность получить схему базы данных
(SQL(DDL)-скрипт для создания таблиц с данными) на его основе создаю бд
и проверяю свои решения прямо на живой бд с нужными данными

Для удобства использую свой плагин для nvim (nvim-env) с возможностью запускать
SQL команды прямо из markdown. (`.dbconf` описывает подключение к бд)
Альтернативный подход для ручной проверки это копирование этих SQL команды и
- либо выполнение их как скриптов через psql -d tmp -f script.sql
- либо прямо в оболочке-клиенте psql через копи-паст
- либо изнутри psql-оболчки через встроенную команду `\i path/to/script.sql`

Важный момент, плагин ходит в бд от лица текущего системного пользователя
и чтобы это работало нужно настроить права доступа.


## Настройка прав доступа и создание базы данных для экспериментов

Через qdb:
(Это cli-тула на чистом bash без внешних зависимостей)
https://gitlab.com/Swarg/dotfiles/-/raw/main/tools/tools/qdb?ref_type=heads

- `qdb mk-dbconf` и вводишь нужные данные для будущего подключения
  (имя субд, имя бд, пользователя и пароль) по этим данным сгенерит файл .dbconf
- затем `qdb create-user-and-db` - создаст прописанного юзера и базу данных с
  установленным владельцем на заданного юзера + права доступа для него.

Либо можно руками:
- создаёшь .dbconf файл, указывая в нём какую субд будешь использовать(драйвер)
название базы данных и пользователя и пароль(параметры подключения)
(пароль можно вынести отдельно в файл ~/.pgpass см доку `qdb doc-refs`)
Дальше создаёшь сразу и нового пользователя (внутри субд) и новую базу данных в
которой и будет вестить спрактика:

Простейший .dbconf
```sh
# per-project configuration
driver=postgres
dbname=db_sql_foundation_sozikin
user=tmp_dbuser
password=tmp_dbuser
```

```sh
qdb check-conf
```
```sh
qdb create-user-and-db

# credentials is ok dbms: postgres db: db_sql_foundation_sozikin user: tmp_dbuser password: has
# postgres db_sql_foundation_sozikin tmp_dbuser *****
```

проверить создалась ли бд с нужным владельцем:
```sh
qdb list-databases  # или просто qdb ls

                                           List of databases
           Name            |   Owner    | Encoding |   Collate   |    Ctype
---------------------------+------------+----------+-------------+-------------
 db_sql_foundation_sozikin | postgres   | UTF8     | en_US.UTF-8 | en_US.UTF-8
 postgres                  | postgres   | UTF8     | en_US.UTF-8 | en_US.UTF-8
...
```

Описания и примеры для чего нужен и как использовать dbconf смотри
[здесь](.practice/old_unix_user_perms.md)



## Задания и краткие описания

- 01-join-two-tables.md
  Пример применения LEFT OUTER JOIN

- 02-delete-duplicate-emails.md
  - Как искать и удалять дубликаты строк в таблице
  - Пример использования подзапросов в команде `DELETE`.
  - Фикс ошибки MySQL: "can't specify target table  for update in FROM clause"

- 03-employees-erning-more.md
  - Объединение таблицы самой с собой
  - Псевдонимы для выводимых стобцов

- 04-join-muliple-tables.md
  - Объединение 3х таблиц. В какие города летал конкретный пассажир
  - Скрипты по созданию заполненной База данных practice/Airo/*.sql

