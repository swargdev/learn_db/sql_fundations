## 181. Employees Earning More Than Their Managers
https://leetcode.com/problems/employees-earning-more-than-their-managers/

```sql
CREATE TABLE IF NOT EXISTS Employee (id INT, name varchar(255), salary INT, managerId INT);
TRUNCATE TABLE Employee;
INSERT INTO Employee (id, name, salary, managerId) VALUES ('1', 'Joe', '70000', '3');
INSERT INTO Employee (id, name, salary, managerId) VALUES ('2', 'Henry', '80000', '4');
INSERT INTO Employee (id, name, salary, managerId) VALUES ('3', 'Sam', '60000', NULL);
INSERT INTO Employee (id, name, salary, managerId) VALUES ('4', 'Max', '90000', NULL); -- 'None'
```


Table: Employee
```
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| id          | int     |
| name        | varchar |
| salary      | int     |
| managerId   | int     |
+-------------+---------+
```
id is the primary key (column with unique values) for this table.
Each row of this table indicates the ID of an employee, their name, salary,
and the ID of their manager.


Write a solution to `find the employees who earn more than their managers`.

Return the result table in any order.
The result format is in the following example.


### Example 1:

Input:
Employee table:
```
+----+-------+--------+-----------+
| id | name  | salary | managerId |
+----+-------+--------+-----------+
| 1  | Joe   | 70000  | 3         |
| 2  | Henry | 80000  | 4         |
| 3  | Sam   | 60000  | Null      |
| 4  | Max   | 90000  | Null      |
+----+-------+--------+-----------+
```
Output:
```
+----------+
| Employee |
+----------+
| Joe      |
+----------+
```
Explanation: Joe is the only employee who earns more than his manager.


## My Solution

Особенность задачи - необходимость объединения таблицы с самой собой.
Здесь таблица `Employee` содержит ссылки на "саму себя". А точнее столбец
`managerId` ссылается своими значениями на другие строки в этой же таблице.
Обычно такую связь еще обозначают как внешний ключ(Foreign Key). И внешним
ключом можно ссылаться не только на другие таблицы но и на эту же самую таблицу
(самого себя)
И хотя в SQL-DDL-скрипте этой задачи такого ограничения для столбца нет,
но смысл такой же) (Это еще называется Degree 1 или "Unary Relationships")

В пример подобные ссылки из колонки `managerid` читаются вот так:
- у `Joe` менеджером является `Sam` т.к. строка c ID=1(Joe) имеет managerId=3
  то есть ссылается на другую строку в этой же таблице на столбец ID. и в этом
  конкретно случае это строка с данными Sam-a
- строка с данными Sam-а (ID=3) говорит о том, что у него менеджера нет, т.к.
  `managerId` = Null(пуст или не задан)
- у `Henry`менеджер `Max`
- у Max нет менеджера как и у Sam

По заданию здесь нужно найти сотрудника у которого зарплата больше чем у его
менеджера и под это подходит Joe (Он же и в ожидаемом правильном ответе)
потому что Joe salary = 70k и Sam salary = 60k

Как обычно по скрипту в начале создаю базу данных с данными из задания.
И проверяю через консоль:

```sql
postgres@[local] tmp=# SELECT * from Employee;
 id | name  | salary | managerid
----+-------+--------+-----------
  1 | Joe   |  70000 |         3
  2 | Henry |  80000 |         4
  3 | Sam   |  60000 |
  4 | Max   |  90000 |
(4 rows)
```

Объединяем таблицу саму с собой задавая условие в `ON` как объединять

```sql
SELECT *
FROM Employee AS e JOIN Employee AS m
  ON e.managerid = m.id;
```
здесь в условии для объединения говорим что нужно объединять по id сотрудника
и его менеджера, и для этого и добавляем второй алиас (`m` - `manager`)

Такая команда даёт вот такой вывод(`SELECT *`):
```
 id | name  | salary | managerid | id | name | salary | managerid
----+-------+--------+-----------+----+------+--------+-----------
 1  | Joe   | 70000  | 3         | 3  | Sam  | 60000  |
 2  | Henry | 80000  | 4         | 4  | Max  | 90000  |
(rows: 2)
```
Здесь видим результат объединения двух таблиц(причем одной и той же)
- первые 4 столбца - данные работника, вторые 4 - его управляющего
- для `Joe` видим его менеджера - `Sam` и его зарплату(60k)

Обрати внимание. В объединённой таблице нет данных о `Sam` и `Max`
(она есть но только в части относящейся к менеджерам а не работникам)
Это так потому что объединяли через `INNER JOIN` (INNER по умолчанию для JOIN)
Как помним для "внутреннего объединения" в результат входят только те строки,
которые есть во второй таблице. А т.к. у Sam и у Max managerId равны NULL то
строки с их данными и не попадают в результат внутреннего объединения.
И это то что нужно, конкретно для этой задачи.

Теперь нужно придумать условия для вывода нужных нам данных
Логически, по условию задачи нужно чтобы `salary` работника была больше чем
у его менеджера.

```sql
SELECT *
FROM Employee AS e JOIN Employee AS m
  ON e.managerid = m.id
WHERE e.salary > m.salary;
```
Результат:
```
 id | name | salary | managerid | id | name | salary | managerid
----+------+--------+-----------+----+------+--------+-----------
 1  | Joe  | 70000  | 3         | 3  | Sam  | 60000  |
(rows: 1)
```
Но нам нужно только имя работника поэтому:

```sql
SELECT e.name
FROM Employee AS e JOIN Employee AS m
  ON e.managerid = m.id
WHERE e.salary > m.salary;
```
Output:
```
 name
------
 Joe
```

Теперь по условию просят имя работника выводить под названием `Employee`
поэтому назначаем кастомное имя для столбца(через псевдоним `AS Employee`):

```sql
SELECT e.name AS Employee
FROM Employee AS e JOIN Employee AS m
  ON e.managerid = m.id
WHERE e.salary > m.salary;
```
```
 employee
----------
 Joe
```

Очистка экспериментальной бд
```sql
DROP TABLE Employee;
```
