-- Deprecated use qdb instead
-- see old_unix_user_perms.md
-- https://gitlab.com/Swarg/dotfiles/-/raw/main/tools/tools/qdb?ref_type=heads
-- Goal:
-- - create permissions to access the database connection for the current
--   system user
--
-- How to use:
-- sudo -i PWD=/tmp -H -u postgres \
--    psql -v dbname=tmp -v sys_user=$USER -f 00-perms.sql

\set ON_ERROR_STOP on
\echo :db_name
\echo :sys_user

-- DROP ROLE IF EXISTS dbuser;
CREATE ROLE dbuser WITH
    LOGIN
    NOSUPERUSER
    NOCREATEDB
    NOCREATEROLE
    NOINHERIT
    NOREPLICATION
    CONNECTION LIMIT -1
    -- PASSWORD :'password_to_save'
;


DROP ROLE IF EXISTS :sys_user;
CREATE ROLE :sys_user WITH LOGIN;
GRANT dbuser TO :sys_user;


CREATE DATABASE :dbname;
ALTER DATABASE :dbname OWNER TO dbuser;

-- access to all tables in public scheme(with created db)
GRANT ALL ON ALL TABLES IN SCHEMA public to dbuser;
-- GRANT ALL PRIVILEGES ON DATABASE :dbname TO dbuser;

-- SET ROLE dbuser;
