-- Авиаперелёты
-- Схема авиаперелётов, связующая пассажиров с проданными билетами
-- CREATE DATABASE IF NOT EXISTS Airo;
-- USE Airo;-- \connect Airo -- for PostreSQL

CREATE TABLE IF NOT EXISTS Company (
  id INT PRIMARY KEY,
  name VARCHAR  -- Название компании-перевозчика
);

CREATE TABLE IF NOT EXISTS Passenger (
  id INT PRIMARY KEY,
  name VARCHAR  -- Имя и фамилия пассажира
);

CREATE TABLE IF NOT EXISTS Trip (
  id INT PRIMARY KEY,
  company INT,  -- Идентификатор компании-перевозчика
  plane VARCHAR,  -- Модель самолёта
  town_from VARCHAR,  -- Город вылета
  town_to VARCHAR,  -- Город прилёта
  time_out TIMESTAMP, -- DATETIME,  -- Время вылета
  time_in TIMESTAMP,  -- Время прилёта

  FOREIGN KEY (company) REFERENCES company(id)
);

CREATE TABLE IF NOT EXISTS Pass_in_trip (
  id INT PRIMARY KEY,
  trip INT,  -- Идентификатор билета
  passenger INT,  -- Идентификатор пассажира
  place VARCHAR,  -- Место пассажира в самолёте

  FOREIGN KEY (trip) REFERENCES trip(id),
  FOREIGN KEY (passenger) REFERENCES passenger(id)
);

