## Удаление строк-дубликатов из таблицы

https://leetcode.com/problems/delete-duplicate-emails/

scheme-ddl.sql
```sql
CREATE TABLE IF NOT EXISTS Person (Id int, Email varchar(255));
TRUNCATE TABLE Person;
INSERT INTO Person (id, email) values ('1', 'john@example.com');
INSERT INTO Person (id, email) values ('2', 'bob@example.com');
INSERT INTO Person (id, email) values ('3', 'john@example.com');
```

Table: Person
```
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| id          | int     |
| email       | varchar |
+-------------+---------+
```
`id` is the primary key (column with unique values) for this table.
Each row of this table contains an email.
The `emails` will not contain uppercase letters.


### Task
Write a solution `to delete all duplicate emails`,
keeping only one unique email with the smallest id.

For SQL users, please note that you are supposed to write a `DELETE` statement
and not a SELECT one.

For Pandas users, please note that you are supposed to modify Person in place.

After running your script, the answer shown is the Person table.
The driver will first compile and run your piece of code and then
show the Person table. The final order of the Person table does not matter.

The result format is in the following example.


## Example 1:

Input:
Person table:
```
+----+------------------+
| id | email            |
+----+------------------+
| 1  | john@example.com |
| 2  | bob@example.com  |
| 3  | john@example.com |
+----+------------------+
```
Output:
```
+----+------------------+
| id | email            |
+----+------------------+
| 1  | john@example.com |
| 2  | bob@example.com  |
+----+------------------+
```

Explanation: john@example.com is repeated two times.
We keep the row with the smallest Id = 1.


## My Solution

Задача решается в два шага
1. сначала нужно спроектировать подзапрос для получения уникальных id email-ов
2. затем оставить в таблице только эти id удалив все остальные(т.е. дубликаты)

1. Проектирование подзапроса
Подзапрос для поиска минимальных id-шников для каждого уникального email-a.
То есть запрос на создание списка ID-строк, которые нужно оставить в БД

Например посмотреть количество повторов каждого уникального email-а можно так:
```sql
SELECT email, COUNT(id) FROM Person GROUP BY email;
```
```
      email       | count
------------------+-------
 bob@example.com  | 1
 john@example.com | 2       << дубликат которы нужно удалить
```

```sql
SELECT MIN(id), email  -- здесь email помещаю для ручной отладки
FROM Person
GROUP BY email;
```
Как это работает
- группировка по email, создавая тем самым список из только уникальных значений
- далее функции MIN(id) в SELECT выдаст только минимальные ID для каждого из
  уникального email-а


Итоговый нужный подзапрос будет таким:
```sql
SELECT MIN(id) FROM Person GROUP BY email;
```

2. Проектирую основной запрос на удаление строк-дубликатов
- нужно удалить все строки с емейлами, ID которых не входит в полученный
  подзапросом список.

```sql
DELETE FROM person
WHERE id NOT IN (SELECT MIN(id) FROM Person GROUP BY email);
```
Как это работает:
- `WHERE id NOT IN(keep_id_list)` - это условие на удаление строк из таблицы
  удалить где id очередной строки не входит(NOT) в(IN) список и далее список
  ID полученный из подзапроса.


## Fix для MySQL
На PostgreSQL такой будет работать сразу, а вот на MySQL выдаст такую ошибку:
- You can't specify target table 'person' for update in FROM clause.

Это из-за особенностей работы MySQL:
- для операций UPDATE и DELETE напрямую внутри WHERE нельзя использовать ту же
  самую таблицу которая используется в самом изменяющем данные действии.
  То есть меняем что-то в таблице и по этой же таблице что-то проверяем через
  подзапрос внутри WHERE условия.

Чтобы это работало на MySQL:
- обходится это через добавление еще одного запроса в подзапросе, внутри
  условия `WHERE` так, чтобы он извлекал все данные из таблицы person, назначая
  псевдоним(alias) для извлечённых данных(строк-таблицы)

```sql
DELETE FROM person
WHERE id NOT IN (
  SELECT * FROM (                              -- доп запрос для оборачивания
    SELECT MIN(id) FROM Person GROUP BY email  -- подзапрос
  ) AS dup_emails                              -- назначаю псевдоним
);
```

Очистка экспериментальной бд
```sql
DROP TABLE Person;
```

Notes:
если в таблице миллионы записей, то `NOT IN` работает очень медленно.
Чтобы ускорить нужно делать JOIN и использовать функцию "row_number() over"
и проиндексировать `email`.
