- Группировка данных
  Ключевое слово `GROUP BY` (в `SELECT` запросе).
  Группировка по одному или нескольким заданным столбцам

- Агрегатные функции
  функции которые применяем к сгруппированым данным.
  COUNT, SUM, AVG, MAX, MIN

- Группировка и другие возможности `SELECT`
  группировку можно сочетать с другими операторами:
  Фильтрация: `WHERE`
  Сортировка: `ORDER BY`
  Ограничение количества строк: `LIMIT`

## Группировка данных

Оператор `SELECT` служит для выборки данных из таблицы, но иногда бывает нужно
исследовать несколько строк отдельно, разделив их на так называемые группы.
То есть сгруппировать строки одной таблицы по некому значению из указанного
столбца а затем к строкам из каждой группы применить некое действие(функцию).

Например в нашей таблице `superheroes` есть женские и мужские персонажи.
Можно например разбить их на две группы по полу и далее выполнить некие
аналитические действия по строкам в каждой из полученных групп.

- самое простое из таких возможных действий - это подсчёт количества строк
  в нашем примере это подсчёт количества женщин и мужчин в таблице.


`GROUP BY` - ключевое слово для группировки данных по некому столбцу

Вот пример группировки по полу т.е. по столбцу с именем `gender`

Подсчитать сколько персонажей конкретного пола есть в таблице `superheroes`:
```sql
SELECT gender, COUNT(*) FROM superheroes
GROUP BY gender;
```

здесь "внутри" SELECT указываем то что хотим получить, а именно:
- `gender` - значение из этого, конкретного поля(столбца) (название пола)
- `COUNT(*)` - функция которую хотим применить к данным (подсчитать количество).

```
         gender         | count
------------------------+-------
 Male Characters        |  4879     мужчины
 Female Characters      |  2373     женщины
 Agender Characters     |    24     безполые
 Genderfluid Characters |     2     способные менять пол
 Genderless Characters  |     3     безполые
(5 rows)
```

Как это работает
- сначала строки группируются в отдельные группы по значению в столбце `gender`.
  То есть группы образуется несколько групп из строк по значениям из поля gender.
- затем к каждой группе применяется заданная функция
  (здесь это подсчёт количества строк-таблицы в группе)

Запрос на получение типов персонажей(сторона) и их количество
```sql
SELECT align, COUNT(*) FROM superheroes
GROUP BY align;
```
```
       align        | count
--------------------+-------
 Good Characters    |  3187
 Neutral Characters |  1169
 Reformed Criminals |     2
 Bad Characters     |  2923
(4 rows)
```


### Многоуровневая группировка данных в SQL

Многоуровневая означает группировка сразу по нескольким столбцам.
Для этого в `GROUP BY` указывается несколько столбцов друг за другом:

```sql
SELECT universe, align, COUNT(*) FROM superheroes
GROUP BY universe, align;
```

`GROUP BY universe, align`
- сначала выполнит группировку по вселенным(universe) (это либо marvel либо dc)
- а затем в каждой из образованных групп идёт разбиение на подгруппы, в
  частности по типу персонажей.

`SELECT universe, align, COUNT(*)`
- сначала указываем все поля по которым идёт группировка
- затем указывается функция для применения к каждой группе
  (здесь используем самую простую из таких доступных функций, а именно -
   подсчёт количества строк-таблицы в группе - функция `COUNT(*)`

Результат выполнения:
```
 universe |       align        | count
----------+--------------------+-------
 marvel   | Bad Characters     |  2011
 marvel   | Neutral Characters |   900
 marvel   | Good Characters    |  1941
 dc       | Reformed Criminals |     2
 dc       | Neutral Characters |   269
 dc       | Good Characters    |  1246
 dc       | Bad Characters     |   912
```
Видим что сначала персонажи разделялись по вселенным(1й столбец), затем
по типу(2й столбец), поэтому в итоге получаем 3 столбца.



### Фильтрация и группировка данных
одним запросом и отфильтровать и сгруппировать данные

Какие цвета волос бывают у женских персонажей и количество таких персонажей
```sql
SELECT hair, COUNT(*) FROM superheroes
WHERE gender='Female Characters'
GROUP BY hair;
```

Здесь:
- сначала идёт фильтрация(выбор) по полу - только женщины.
- затем идёт группировка по цвету волос
- затем подсчёт количества строк в каждой группе

```
         hair          | count
-----------------------+-------
 Strawberry Blond Hair |    25
 Violet Hair           |     3
 Bald                  |    18
 Green Hair            |    42
 Silver Hair           |     6
 Red Hair              |   272
 White Hair            |    97
 Purple Hair           |    28
 Pink Hair             |    19
 Grey Hair             |    39
 Yellow Hair           |     2
 Black Hair            |   799
 Gold Hair             |     1
 Magenta Hair          |     3
 Blond Hair            |   515
 Reddish Brown Hair    |     1
 No Hair               |    44
 Auburn Hair           |    26
 Variable Hair         |     4
 Platinum Blond Hair   |     2
 Orange Hair           |     7
 Blue Hair             |    17
 Brown Hair            |   403
(23 rows)
```

Отсортировать цвета волос по популярности
```sql
SELECT hair, COUNT(*) FROM superheroes
WHERE gender='Female Characters'
GROUP BY hair
ORDER BY count(*) DESC; -- отсортировать данные по столбцу с именем count(*)
                        -- в порядке убывания DESC
```

```
        hair          | count
-----------------------+-------
 Black Hair            |   799
 Blond Hair            |   515
 Brown Hair            |   403
 Red Hair              |   272
 White Hair            |    97
 No Hair               |    44
 Green Hair            |    42
 Grey Hair             |    39
 Purple Hair           |    28
 Auburn Hair           |    26
 Strawberry Blond Hair |    25
 Pink Hair             |    19
 Bald                  |    18
 Blue Hair             |    17
 Orange Hair           |     7
 Silver Hair           |     6
 Variable Hair         |     4
 Magenta Hair          |     3
 Violet Hair           |     3
 Platinum Blond Hair   |     2
 Yellow Hair           |     2
 Gold Hair             |     1
 Reddish Brown Hair    |     1
(23 rows)
```

top-5 цветов волос(через ограничение - `LIMIT`)
```sql
SELECT hair, COUNT(*) FROM superheroes
WHERE gender='Female Characters'
GROUP BY hair
ORDER BY count(*) DESC
LIMIT 5;                            -- ограничение по количеству строк
```
Это покажет тоже самое, но только выдаст первые 5 строк-таблицы а не все возможные.
